// --- Exercicio 16 ---
// Lindovaldo Costa Leão

var diario_eletronico = [];

function media(veri) { if (veri.nota < 60) { return veri; } }

while (confirm("Deseja adicionar um novo aluno ?")) {
  let ficha = {
    nome: prompt("Informe o nome: "),
    nota: Number(prompt("Informe a nota: ")),
    bonus: confirm("Aluno tem bonus ?"),
  }
  diario_eletronico.push(ficha);
}

diario_eletronico.sort((a, b) => (a.nota > b.nota) ? -1 : ((b.nota > a.nota) ? 1 : 0) );// Ordena notas maiores para menores 

console.log("Todos Alunos: ");
diario_eletronico.forEach(function (imp) {
  console.log(`Nome: ${imp.nome}`);
  console.log(`Nota: ${imp.nota}`);
  console.log(`Bonus: ${imp.bonus}\n`);
});

diario_eletronico.map(function (uti_bonus) {
  if (uti_bonus.bonus) {
    if (uti_bonus.nota >= 55 && uti_bonus.nota < 60) {
      uti_bonus.nota = 60;
    } else if (uti_bonus.nota > 60) {
      uti_bonus.nota = uti_bonus.nota * (100 + 10) / 100;
      if (uti_bonus.nota > 100) {
        uti_bonus.nota = 100;
      }
    }
  }
});

let reprovado = diario_eletronico.filter(media);
console.log("Alunos que foram reprovados:\n");
reprovado.forEach(function (imp) {
  console.log(`Nome: ${imp.nome}`);
  console.log(`Nota: ${imp.nota}`);
  console.log(`Bonus: ${imp.bonus}\n`);
});


let notas = [];
diario_eletronico.forEach((n) => notas.push(n.nota));

const nota_media = notas.reduce((acum, elem) => acum + elem) / notas.length;// Tira a media das notas
console.log("Nota media da turma:", nota_media);