// --- Exercicio 13 ---
// Lindovaldo Costa Leão

let diario_eletronico = [];

function media(veri){if(veri.nota < 60){return veri;}}

while(confirm("Deseja adicionar um novo aluno ?")){
  let ficha = {
    nome: prompt("Informe o nome: ") ,
    nota: prompt("Informe a nota: "),
  }
  diario_eletronico.push(ficha);
}

console.log("Alunos: ");
diario_eletronico.forEach(function(imp){
  console.log(`Nome: ${imp.nome}`);
  console.log(`Nota: ${imp.nota}\n`);
});

let reprovado = diario_eletronico.filter(media);

console.log("Alunos que foram reprovados:\n");
reprovado.forEach(function(imp){
  console.log(`Nome: ${imp.nome}`);
  console.log(`Nota: ${imp.nota}\n`);
});