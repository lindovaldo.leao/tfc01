// --- Exercicio 14 ---
// Lindovaldo Costa Leão

let diario_eletronico = [];

function media(veri){if(veri.nota < 60){return veri;}}

while(confirm("Deseja adicionar um novo aluno ?")){
  let ficha = {
    nome: prompt("Informe o nome: ") ,
    nota: prompt("Informe a nota: "),
    bonus: confirm("Aluno tem bonus ?"),
  }
  diario_eletronico.push(ficha);
}


console.log("Todos Alunos: ");
diario_eletronico.forEach(function(imp){
  console.log(`Nome: ${imp.nome}`);
  console.log(`Nota: ${imp.nota}`);
  console.log(`Bonus: ${imp.bonus}\n`);
});

diario_eletronico.map(function(uti_bonus){
  if(uti_bonus.bonus){
    if(uti_bonus.nota >= 55 && uti_bonus.nota < 60){
      uti_bonus.nota = 60;
    }else if(uti_bonus.nota > 60){
      uti_bonus.nota = uti_bonus.nota * (100 + 10)/100;
      if(uti_bonus.nota > 100){
        uti_bonus.nota = 100;
      }
    }
  }
});

let reprovado = diario_eletronico.filter(media);

console.log("Alunos que foram reprovados:\n");
reprovado.forEach(function(imp){
  console.log(`Nome: ${imp.nome}`);
  console.log(`Nota: ${imp.nota}`);
  console.log(`Bonus: ${imp.bonus}\n`);
});