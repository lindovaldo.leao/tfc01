// --- Exercicio 12 --- 
// Lindovaldo Costa Leão


let a = Number(prompt("Informe um numero para A: "));
let b = Number(prompt("Informe um numero para B: "));
let w = String(prompt("Informe uma string para W: "));

function sum(num1 ,num2){
  if(num1 > num2){
    return "Valor de A deve ser menor que B.";
  }else{
    const menor =  num1;
    const maior =  num2;

    let i, soma = 0;
    for(i = menor; i <= maior; i++){
      soma = soma + i;
    }

    return soma;
  }
  
}

function mul(num1, num2){
  if(num1 > num2){
    return "Valor de A deve ser menor que B.";
  }else{
    const menor =  num1;
    const maior =  num2;

    let i, produto = 1;
    for(i = menor; i <= maior; i++){
      produto = produto * i;
    }
    
    return produto;
  }
}

function div(num1, num2){
  if(num1 > num2){
    return "Valor de A deve ser menor que B.";
  }else{
    const menor =  num1;
    const maior =  num2;

    let i, quociente = 1;
    for(i = menor; i <= maior; i++){
      quociente = quociente / i;
    }
    
    return quociente;
  }
}

if(w == "SUM"){console.log(`SUM[${a},${b}] = ${sum(a,b)}`)}
else if(w == "MUL"){console.log(`MUL[${a},${b}] = ${mul(a,b)}`)}
if(w == "DIV"){console.log(`DIV[${a},${b}] = ${div(a,b)}`)}