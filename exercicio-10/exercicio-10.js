 /*  ******************************************************* Exercicio 10 ******************************************************* */
// Lindovaldo Costa Leão

let num1 = Number(prompt("Informe um valor inteiro:")) ;

while(Number.isNaN(num1) === true){
  num1 = Number(prompt("Valor de entrada incorreto, tente novamente."));
} 

let operador = String(prompt("Informe um sinal e operação:")) ;

while(operador != '+' && operador != '-' && operador != '*' && operador != '/'){
  operador = prompt(`Operação "${operador}" não existe, tente novamente`) ;
}

let num2 = Number(prompt("Informe um valor inteiro:")) ;

while(Number.isNaN(num2) === true){
  num2 = Number(prompt("Valor de entrada incorreto, tente novamente."));
}

let resu;

if(operador == '+' ){
  resu = num1 + num2;
}else if(operador == '-'){
  resu = num1 - num2;
}else if(operador == '*'){
  resu = num1 * num2;
}else if(operador == '/'){
  resu = num1 / num2;
}

console.log(`${num1} ${operador} ${num2} = ${resu}`);
