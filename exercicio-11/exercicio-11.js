
// --- Exercicio 11 ---
// Lindovaldo Costa Leão

let w = prompt("Escolha uma função [ASC, DESC, INTER]");

if(w != 'ASC' && w != 'DESC' && w != 'INTER'){
  alert("Erro");
}else if(w == 'ASC'){
  umDez();
}else if(w == 'DESC'){
  dezUm();
}else{
  inter();
}

function umDez(){// ASC
  for(let i = 1; i <= 10; i++){
    console.log(`${i}`);
  }
}// ASC

function dezUm(){// DESC
  for(let i = 10; i >= 1; i--){
    console.log(`${i}`);
  }
}// DESC

function inter(){// INTER
  for(i = 0; i < 10; i++){
    console.log(parseInt(Math.random() * (1 * 10) - 1));
  }  
}// INTER
