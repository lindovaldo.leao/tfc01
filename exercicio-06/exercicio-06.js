let entrada1 = 10;
let entrada2 = 20;
let entrada3 = 20;
let entrada4 = 0;
let total = entrada1 + entrada2 + entrada3 + entrada4;

if(entrada1 > 15)
  console.log('Erro: atividade 1 não pode ter mais que 15 pontos lançados!');

if(entrada2 > 25)
  console.log('Erro: atividade 2 não pode ter mais que 25 pontos lançados!');

if(entrada3 > 25)
  console.log('Erro: atividade 3 não pode ter mais que 25 pontos lançados!');

if(entrada4 > 35)
  console.log('Erro: atividade 4 não pode ter mais que 35 pontos lançados!');

if (total >= 60)
  console.log('O aluno foi aprovado');
else
  console.log('O aluno foi reprovado, faltando', 60 - total ,' pontos para a pontuação mínima de aprovação');
